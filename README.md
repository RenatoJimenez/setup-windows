# README #

### What is this repository for? ###

This repository holds utility files that can be used for convenience when switching to another development machine (Windows only). It contains my personal preference of Win's command line prompt appearance as well as batch files that can be used to launch my common must-have applications.

### How do I get set up? ###

#### CMD Appeareance ####
    1) Open cmd prompt on Windows
    2) Select properties
    3) Copy the options from terminal-win.txt

#### Setup Batch Files ####
    1) Go to "Edit environmental variables" on your windows machine
    2) Go to "Advanced" tab
    3) Select "Environmental variables..."
    4) On the "System variables" select "Path" and "Edit" it
    5) Add "%repository_path%\shortcuts\" as a variable 

Edit %repository_path% with the path to this repository project